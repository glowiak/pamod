SRC=	pamod
PREFIX=	/usr
DESTDIR=	

all:
	@echo Type 'make install' as root to install ${SRC}

install:
	mkdir -pv ${DESTDIR}${PREFIX}/bin
	mkdir -pv ${DESTDIR}/etc
	cp -v ./${SRC} ${DESTDIR}${PREFIX}/bin/
	chmod 775 ${DESTDIR}${PREFIX}/bin/${SRC}
	cp -v ./${SRC}.conf ${DESTDIR}/etc/${SRC}.conf

uninstall:
	rm -rf ${DESTDIR}${PREFIX}/bin/${SRC}
	rm -rf ${DESTDIR}/etc/${SRC}.conf
